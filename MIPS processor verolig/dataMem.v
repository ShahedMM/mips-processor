module dataMem (clk,address,write_data,mem_write_en,read_data,byte_length);
	`include "param.v"
	input clk, mem_write_en,byte_length;
	input [`widthReg-1:0] address, write_data;
	
	output [`widthReg-1:0] read_data;

  integer i;
  reg [7:0] dataMem [1023:0];
 
  initial 
  begin
	for( i = 0;i<1024;i=i+1)
		dataMem[i] <= 8'b0;
	dataMem[7] <= 8'b00001000;
	//dataMem[8] <= 8'b00000111;
	//dataMem[10] <= 8'b00001000;
    end

  always @ (negedge clk) begin
    if (mem_write_en==1) begin
    	if(byte_length == 0)
	    	{dataMem[address], dataMem[address + 1], dataMem[address + 2], dataMem[address + 3]} <= write_data;
	    else
	    	dataMem[address + 3] <= write_data[7:0];
	    $monitor("DataMem[%d] = %d",address,write_data);
	end
  end

  assign read_data = (byte_length == 0) ? {dataMem[address], dataMem[address + 1], dataMem[address + 2], dataMem[address + 3]} : {24'b0, dataMem[address + 3]};

endmodule 