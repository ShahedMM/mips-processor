//widths
`define widthReg 32
`define selReg 5
`define aluOp 4
`define opcode_func 6


//opcode 
`define R  6'h01
`define AddImmediate 6'h9
`define AddImmediateUnsigned 6'h8
`define AndImmediate 6'hc
`define OrImmediate 6'he
`define LoadWord 6'h12
`define StoreWord 6'h2b
`define Branch 6'h5
`define BranchNotEqual 6'h4
`define Jump 6'h2
`define JumpAndLink 6'h3
`define AddFloat 6'h11
`define LoadWordF 6'h31
`define LoadByteUnsigned 6'h22
`define SotreByte 6'h28
`define LoadUpperImm 6'hf


//Funct 
`define AddReg 6'h20
`define AndReg  6'h14
`define SubReg 6'h24
`define SubuReg 6'h22
`define AddRegF 6'h0
`define MoveHigh 6'h10
`define MoveLow 6'h12
`define Multiply 6'h18
`define Divide 6'h1a
`define OrReg 6'h25
`define NorReg 6'h27
`define SltReg 6'h2a
`define SltuReg 6'h2b
`define SllReg 6'h00 
`define SlrReg 6'h02
`define LwNew 6'h21
`define SwNew 6'h13
`define JumpReg 6'h8

//ALU operations
`define Add 4'h0
`define Sub 4'h1
`define And 4'h2
`define AddF 4'h3  //not from sheet 
`define Mfhi 4'h4
`define Mflo 4'h5
`define Mult 4'h6
`define Div 6'h7
`define Or 4'h8
`define Nor 4'h9
`define Slt 4'ha
`define Sltu 4'hb
`define Sll 4'hc
`define Slr 4'hd
`define Addu 4'he
`define Subu 4'hf


