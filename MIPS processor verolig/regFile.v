module regFile (clk, read_reg1, read_reg2, read_reg3, reg_write, write_data, reg_write_en, read_data1, read_data2, read_data3);
  input clk, reg_write_en;
  input [4:0] read_reg1, read_reg2, read_reg3, reg_write;
  input [31:0] write_data;
  output [31:0] read_data1, read_data2, read_data3;

  reg [31:0] reg_file [31:0];
	initial 
	begin
    reg_file[0] = 32'h0000; //zero
    reg_file[8] = 32'h0003; //t0
    reg_file[9] = 32'h0008; //t1
    reg_file[10] = 32'hfffffff1; //-15 //t2
    reg_file[11] = 32'hfffffffa; //t3
    reg_file[12] = 32'h00007; //t4
    reg_file[13] = 32'h00004; //t5
    reg_file[14] = 32'h00002; //t6
    reg_file[15] = 32'h00001; //$t7
	end
	
  always @ (negedge clk) begin
    if (reg_write_en==1) 
		reg_file[reg_write] <= write_data;
  end


  always @(*)begin
  	//$monitor("reg_file[12]: %d, reg_file[14]: %d",reg_file[12],reg_file[14]);
  end


  assign read_data1 = reg_file[read_reg1];
  assign read_data2 = reg_file[read_reg2];
  assign read_data3 = reg_file[read_reg3];
endmodule