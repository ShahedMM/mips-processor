module regFileFloat(clk, read_reg1, read_reg2, reg_write, write_data, reg_write_en, read_data1, read_data2);
  input clk, reg_write_en;
  input [4:0] read_reg1, read_reg2, reg_write;
  input [31:0] write_data;
  output [31:0] read_data1, read_data2;

  reg [31:0] reg_file [31:0];
	initial 
	begin
		reg_file[0] = 32'h0009;
		reg_file[1] = 32'h0003;
		reg_file[2] = 32'b10000001110000000000000000000000;
		reg_file[3] = 32'b10000011101100000000000000000000;
		
	end
	
  always @ (negedge clk) begin
    if (reg_write_en) 
		reg_file[reg_write] <= write_data;
		
  end

  assign read_data1 = reg_file[read_reg1];
  assign read_data2 = reg_file[read_reg2];
endmodule