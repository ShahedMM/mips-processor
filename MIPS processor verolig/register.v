`include "param.v"
module register (clk, regIn, regOut , en);
  input clk, en;
  input [`widthReg-1:0] regIn;
  output reg [`widthReg-1:0] regOut;
	initial begin
		regOut = 32'b0;
	end
  always @ (posedge clk)  begin
  if(en)
    begin
    regOut <= regIn;
    end
  end
endmodule // register