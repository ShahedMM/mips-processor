module alu(alu_op,in1,in2,result,zero);
	`include "param.v"
	input [`aluOp-1:0]alu_op;
	input [`widthReg-1:0]in1,in2;
	
	output reg [`widthReg-1:0] result;
	output reg zero;
	
	reg [`widthReg-1:0] high,low; //high and low registers are stored in alu instead of register file
	wire [`widthReg-1:0] result_f;
	wire [`widthReg+`widthReg-1:0] result_mul,result_div; //2*widthReg
	aluMult aluMult(
		.in1(in1),
		.in2(in2),
		.result(result_mul)
	);
	aluDiv aluDiv(
		.in1(in1),
		.in2(in2),
		.result(result_div)
	);
	aluAddF aluAddF(
		.in1(in1),
		.in2(in2),
		.result(result_f)
	);
	always @(*)begin
		case (alu_op)
			`Add: 	result <= $signed(in1) + $signed(in2);
			`Addu: 	result <= in1 + in2;
			`And:	result <= in1 & in2;
			`Or: 	result <= in1 | in2;
			`Nor: 	result <= ~(in1 | in2);
			`Sub: 	result <= $signed(in2) - $signed(in2);
			`Subu: 	result <= in2 - in1;
			`Slt: 	result <= $signed(in1) < $signed(in2);
			`Sltu: 	result <= in1 < in2;
			`Sll: 	result <= in1 << in2;
			`Slr: 	result <= in1 >> in2;
			`AddF: #1 result <= result_f;
			`Mfhi: result <= high;
			`Mflo: result <= low;
			`Mult: {high,low} <= result_mul;
			`Div: {high,low} <= result_div;
			default: result <= 0;
		endcase
		zero <= result==0;
	$monitor("alu_op: %d in1: %d, in2: %d, result: %d",alu_op,in1,in2,result);
	end
endmodule
