 module ID2EXE(clk, read_data1_in,read_data2_in,immediate_in,write_reg_in1,write_reg_in2,write_reg_in3,write_back_in,mem_to_reg_in,alu_op_in,alu_src_in,write_mem_in,read_data1,read_data2,immediate,write_reg1,write_reg2,write_reg3,write_back,mem_to_reg,alu_src,write_mem,alu_op,reg_dst_in,reg_dst,branch_in,branch,pc_in,pc,jump_in,j_address_in,jump,j_address,bne_in,bne,read_data1F_in, read_data2F_in,read_data1F, read_data2F,reg_float_in,reg_float,reg_Rs,reg_Rs_in,reg_Rt,reg_Rt_in,use_shamt_in,use_shamt,shamt_in,shamt,new_signal,new_signal_in,read_data3,read_data3_in,byte_length_in,byte_length,upp_imm_in,upp_imm,jr_in,jr,pc_8_in,pc_8,link_in,link);
	`include "param.v"
  input clk;
  input [`widthReg-1:0] read_data1_in, read_data2_in, read_data3_in,immediate_in, pc_in, j_address_in,read_data1F_in, read_data2F_in,reg_float_in,shamt_in,pc_8_in;
  input [`selReg-1:0] reg_Rs_in,reg_Rt_in;
  input [`selReg-1:0] write_reg_in1,write_reg_in2,write_reg_in3;
  input mem_to_reg_in,alu_src_in,write_mem_in,branch_in,jump_in,bne_in,use_shamt_in,new_signal_in,byte_length_in,upp_imm_in,jr_in, link_in;
  input [`aluOp-1:0] alu_op_in;
  input [1:0]reg_dst_in,write_back_in;

  
  output reg [`widthReg-1:0] read_data1, read_data2, read_data3, read_data1F, read_data2F, immediate, pc, j_address, shamt, upp_imm, pc_8;
  output reg [`selReg-1:0] reg_Rs,reg_Rt;
  output reg [`selReg-1:0] write_reg1,write_reg2,write_reg3;
  output reg mem_to_reg,alu_src,write_mem,branch,jump,bne,reg_float,use_shamt,new_signal,byte_length,jr,link;
  output reg [`aluOp-1:0] alu_op;
  output reg [1:0] reg_dst,write_back; 

  always @ (posedge clk) begin
     read_data1 <= read_data1_in;
	 read_data2 <= read_data2_in;
	 read_data3 <= read_data3_in;
	 read_data1F <= read_data1F_in;
	 read_data2F <= read_data2F_in;
	 immediate <= immediate_in;
	 write_reg1 <= write_reg_in1;
	 write_reg2 <= write_reg_in2;
	 write_reg3 <= write_reg_in3;
	 alu_op <= alu_op_in;
	 write_back <= write_back_in;
	 mem_to_reg <= mem_to_reg_in;
	 alu_src <= alu_src_in;
	 use_shamt <= use_shamt_in;
	 shamt <= shamt_in;
	 write_mem <= write_mem_in;
	 reg_dst <= reg_dst_in;
	 branch <= branch_in;
	 bne <= bne_in;
	 pc <= pc_in;
	 jump <= jump_in;
     j_address<= j_address_in;
	 reg_float <= reg_float_in;
	 reg_Rs <= reg_Rs_in;
	 reg_Rt <= reg_Rt_in;
	 new_signal <= new_signal_in;
	 byte_length <= byte_length_in;
	 upp_imm <= upp_imm_in;
	 jr <= jr_in;
	 link <= link_in;
	 pc_8 <= pc_8_in;

  end
endmodule