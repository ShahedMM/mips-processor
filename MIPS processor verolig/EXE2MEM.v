module EXE2MEM(clk,alu_result_in,alu_result,write_data_mem_in,write_data_mem,write_back_in,write_back,write_mem_in,write_mem,write_reg_in,write_reg,mem_to_reg_in,mem_to_reg,byte_length_in,byte_length,upp_imm_in,upp_imm,immediate_in,immediate,pc_8_in,pc_8,link_in,link);
	`include "param.v"
	input clk,write_mem_in,mem_to_reg_in,byte_length_in,upp_imm_in,link_in;
	input [`widthReg-1:0] alu_result_in,write_data_mem_in,immediate_in,pc_8_in;
	input [`selReg-1:0] write_reg_in;
	input [1:0 ]write_back_in;

	output reg write_mem,mem_to_reg,byte_length,upp_imm,link;
	output reg [`widthReg-1:0] alu_result,write_data_mem,immediate,pc_8;
	output reg [`selReg-1:0] write_reg;
	output reg [1:0]write_back;
	
	
	always @(posedge clk) begin
		alu_result <= alu_result_in; 
		write_reg <= write_reg_in;
		write_data_mem <= write_data_mem_in;
		write_back <= write_back_in;
		write_mem <= write_mem_in;
		mem_to_reg <= mem_to_reg_in;
		byte_length <= byte_length_in;
		immediate <= immediate_in;
		upp_imm <= upp_imm_in;
		link <= link_in;
	 	pc_8 <= pc_8_in;
	end
	
endmodule
