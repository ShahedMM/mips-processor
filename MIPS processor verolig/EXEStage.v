module EXStage(clk,read_data1,reg_Rs,reg_Rt,read_data2,read_data3,new_signal,read_data1F,read_data2F,reg_float,immediate,
	alu_op,alu_src,use_shamt,shamt,alu_result,write_reg1,write_reg2,write_reg3,write_reg,reg_dst,pc,branch,bne,PCsrc,branch_pc,
	write_back_EXE,reg_Rd_EXE,write_back_MEM,reg_Rd_MEM,fwd_write_data_EXE,fwd_write_data_MEM,jr,j_address,j_address_in,write_mem,write_data_in,write_data);
	`include "param.v"
	input clk,alu_src,branch,bne,reg_float,use_shamt,new_signal,jr,write_mem;
	input [`widthReg-1:0]read_data1,read_data2,read_data3,read_data1F,read_data2F,immediate, pc,fwd_write_data_EXE,fwd_write_data_MEM,shamt, j_address_in, write_data_in;
	input [`aluOp-1:0]alu_op;
	input [`selReg-1:0] write_reg1,write_reg2,write_reg3,reg_Rs,reg_Rt,reg_Rd_EXE,reg_Rd_MEM;
	input [1:0] reg_dst,write_back_EXE,write_back_MEM;
	
	wire [`widthReg-1:0] alu_input1,alu_input_tmp,alu_input2;
	wire [1:0] fwd_A,fwd_B;
	wire fwd_C;
	wire [`widthReg-1:0] branch_displacement,zero,PCsrc1,PCsrc2,final_alu_input1,final_alu_input2,alu_input_tmp2,alu_input_tmp3;

	output [`widthReg-1:0] alu_result, branch_pc, j_address, write_data;
	output [`selReg-1:0] write_reg;
	output PCsrc;

	shiftLeft displacement(
		.in(immediate),
		.bits(2),
		.out(branch_displacement)
	);

	adder2_1 branch_pc_adder(
		.in1(branch_displacement),
		.in2(pc),
		.out(branch_pc)
	);
	
	and2_1 branchOnEqualTaken(  
		.in1(branch),
		.in2(zero),
		.out(PCsrc1)
	);

	and2_1 branchNotEqualTaken(  
		.in1(bne),
		.in2(~zero),
		.out(PCsrc2)
	);

	or2_1 branchTaken(
		.in1(PCsrc1),
		.in2(PCsrc2),
		.out(PCsrc)
	);

	mux2_1 mux2_1JumpReg( 
		.sel(jr),
		.in1(j_address_in),
		.in2(read_data1),
		.out(j_address)
	);

	mux2_1 mux2_1ReadReg1(
		.sel(reg_float),
		.in1(read_data1),
		.in2(read_data1F),
		.out(alu_input1)
	);
	
	mux2_1 mux2_1ReadReg2(
		.sel(reg_float),
		.in1(read_data2),
		.in2(read_data2F),
		.out(alu_input_tmp)
	);
	
	mux2_1 mux2_1EXE(
		.sel(alu_src),
		.in1(alu_input_tmp),
		.in2(immediate),
		.out(alu_input_tmp2)
	);

	mux2_1 mux2_1Shamt(
		.sel(use_shamt),
		.in1(alu_input_tmp2),
		.in2(shamt),
		.out(alu_input_tmp3)
	);

	mux2_1 mux2_1new_signal(
		.sel(new_signal),
		.in1(alu_input_tmp3),
		.in2(read_data3),
		.out(alu_input2)
	);

	mux3_1 mux3_1RegSel( 
		.sel(reg_dst),
		.in1(write_reg1),
		.in2(write_reg2),
		.in3(write_reg3),
		.out(write_reg)
	);

	mux3_1_data mux3_1AluFinalInput1(
		.sel(fwd_A),
		.in1(alu_input1),
		.in2(fwd_write_data_MEM),
		.in3(fwd_write_data_EXE),
		.out(final_alu_input1)
	);
	mux3_1_data mux3_1AluFinalInput2(
		.sel(fwd_B),
		.in1(alu_input2),
		.in2(fwd_write_data_MEM),
		.in3(fwd_write_data_EXE),
		.out(final_alu_input2)
	);

	mux2_1 mux2_1FinalWriteData(
		.sel(fwd_C),
		.in1(write_data_in),
		.in2(fwd_write_data_MEM),
		.out(write_data)
	);

	forwardingUnit fwd1( 
		.reg_Rs(reg_Rs),
		.reg_Rt(reg_Rt),
		.reg_Rd_EXE(reg_Rd_EXE),
		.reg_Rd_MEM(reg_Rd_MEM),
		.write_back_EXE(write_back_EXE),
		.write_back_MEM(write_back_MEM),
		.write_mem(write_mem),
		.fwd_A(fwd_A),
		.fwd_B(fwd_B),
		.fwd_C(fwd_C)
	);
	
	alu aluEXE(		
		.alu_op(alu_op),
		.in1(final_alu_input1),
		.in2(final_alu_input2),
		.result(alu_result),
		.zero(zero)
	);
endmodule
