module forwardingUnit(reg_Rs,reg_Rt,reg_Rd_EXE,reg_Rd_MEM,write_back_EXE,write_back_MEM,fwd_A,fwd_B,write_mem,fwd_C);
	`include "param.v"
	input [`selReg-1:0]  reg_Rs,reg_Rt,reg_Rd_EXE,reg_Rd_MEM;
	input [1:0] write_back_EXE,write_back_MEM;
	input write_mem;

	output reg [1:0] fwd_A,fwd_B;
	output reg fwd_C;
	reg tmp;
	always @(*)begin

		fwd_A = 0;
		fwd_B = 0;
		fwd_C = 0;

		if(write_back_EXE==1 && reg_Rd_EXE != 0 && reg_Rd_EXE == reg_Rs)
			fwd_A = 10;

		if(write_back_EXE==1 && reg_Rd_EXE != 0 && reg_Rd_EXE == reg_Rt)
			fwd_B = 10;

		if(write_back_MEM==1 && reg_Rd_MEM != 0 && 
			!(write_back_EXE==1 && reg_Rd_EXE != 0 && reg_Rd_EXE == reg_Rs) && 
			reg_Rd_MEM == reg_Rs)
			    fwd_A = 01;

		if(write_back_MEM==1 && reg_Rd_MEM != 0 && 
			!(write_back_EXE==1 && reg_Rd_EXE != 0 && reg_Rd_EXE == reg_Rt) && 
			reg_Rd_MEM == reg_Rt)
			    fwd_B = 01;

		if(write_mem == 1 && fwd_B == 01)begin
			fwd_B = 0;
			fwd_C = 1;
		end
	end
endmodule
