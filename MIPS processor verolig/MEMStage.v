module MEMStage(clk,address,write_data,mem_write_en,read_data,byte_length);
	`include "param.v"
	input clk, mem_write_en,byte_length;
	input [`widthReg-1:0] address, write_data;
	
	output [`widthReg-1:0] read_data;

	dataMem dataMem(
		.clk(clk),
		.address(address),
		.write_data(write_data),
		.mem_write_en(mem_write_en),
		.byte_length(byte_length),
		
		.read_data(read_data)
	);

endmodule
	