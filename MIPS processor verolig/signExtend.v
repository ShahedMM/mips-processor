module signExtend(in,out);
	`include "param.v"
	input [15:0] in;
	output reg [`widthReg-1:0] out;
	
	always@(in)begin
		out = { {16{in[15]}}, in[15:0] };
	end
	
endmodule
