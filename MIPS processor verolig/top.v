`include "param.v"
module top(clk);
	input clk;
	//IF
	wire [`widthReg-1:0] pc_in_IF,inst_in,pc_8_in_IF;
	wire [`widthReg-1:0] pc_IF,inst,pc_8_IF;
	
	
	//ID
	wire [`widthReg-1:0] read_data1_in,read_data2_in,read_data3_in,read_data1F_in,read_data2F_in,immediate_in_ID, shamt_in, shamt, link_in_ID;
	wire [`aluOp-1:0] alu_op_in,alu_op;
	wire [`widthReg-1:0] read_data1,read_data2,read_data3,read_data1F,read_data2F,immediate_ID,pc_ID,j_address_in,j_address,pc_8_ID;
	wire write_mem_in_ID,alu_src_in,mem_to_reg_in_ID,new_signal_in,new_signal,byte_length_in_ID,byte_length_ID,upp_imm_in_ID,upp_imm_ID,jr_in,jr,link_ID;
	wire write_mem_ID,alu_src_ID,mem_to_reg_ID,branch_in,branch,jump_in,jump,bne_in,bne,reg_float_in,reg_float,use_shamt_in,use_shamt;
	wire [`selReg-1:0 ]write_reg_ID1,write_reg_ID2,write_reg_ID3,reg_Rs_in,reg_Rs,reg_Rt_in,reg_Rt;
	wire [1:0] write_back_in_ID,write_back_ID,reg_dst_in,reg_dst;
	
	//EX Stage
	wire [`widthReg-1:0] alu_result_in_EXE,alu_result_EXE,write_data_mem_in,write_data_mem,branch_pc,immediate_EXE,j_address_final,pc_8_EXE;
	wire write_mem_in_EXE,write_mem_EXE,mem_to_reg_EXE,PCsrc,byte_length_EXE,upp_imm_EXE,link_EXE;
	wire [`selReg-1:0] write_reg_EXE,write_reg_EXE_in;
	wire [1:0] write_back_EXE;
	
	//MEM Stage
	wire [`widthReg-1:0] read_data_mem_in,read_data_mem,alu_result_MEM,immediate_MEM,pc_8_MEM;
	wire [`selReg-1:0] write_reg_in_MEM, write_reg_MEM;
	wire mem_to_reg_MEM,pc_write,reg_IF2ID_write,upp_imm_MEM,link_MEM;
	wire [1:0] write_back_MEM;
	
	
	//WB Stage
	wire [`widthReg-1:0] write_data_reg,write_data;
	

	//pipline stages
	IFStage IFStage(
		.clk(clk),
		.pc(pc_in),
		.inst(inst_in),
		.pc_next(pc_in_IF),
		.PCsrc(PCsrc),
		.branch_pc(branch_pc),
		.jump(jump),
		.j_address(j_address_final),
		.pc_write(pc_write),
		.pc_8(pc_8_in_IF)
	);
	IDStage IDStage(
		.clk(clk),
		.inst(inst),
		.reg_write(write_reg_MEM),
		.reg_write_en(write_back_MEM),
		.write_data(write_data_reg),
		.pc(pc_IF),

		.mem_to_reg_EXE(mem_to_reg_ID),
		.reg_write_EXE(write_reg_ID1),
		
		.read_data1(read_data1_in),
		.read_data2(read_data2_in),
		.read_data3(read_data3_in),
		.read_data1F(read_data1F_in),
		.read_data2F(read_data2F_in),
		.reg_float(reg_float_in),
		.immediate(immediate_in_ID),
		.upp_imm(upp_imm_in_ID),
		.alu_op(alu_op_in),
		.write_back(write_back_in_ID),
		.alu_src(alu_src_in),
		.use_shamt(use_shamt_in),
		.shamt(shamt_in),
		.write_mem(write_mem_in_ID),
		.mem_to_reg(mem_to_reg_in_ID),
		.reg_dst(reg_dst_in),
		.branch(branch_in),
		.bne(bne_in),
		.jump(jump_in),
		.j_address(j_address_in),
		.reg_Rs(reg_Rs_in),
		.reg_Rt(reg_Rt_in),
		.pc_write(pc_write),
		.reg_IF2ID_write(reg_IF2ID_write),
		.new_signal(new_signal_in),
		.byte_length(byte_length_in_ID),
		.jr(jr_in),
		.link(link_in_ID)
	);
	EXStage EXStage(
		.clk(clk),
		.read_data1(read_data1),
		.read_data2(read_data2),
		.read_data3(read_data3),
		.read_data1F(read_data1F),
		.read_data2F(read_data2F),
		.reg_float(reg_float),
		.immediate(immediate_ID),
		.alu_op(alu_op),
		.alu_src(alu_src_ID),
		.use_shamt(use_shamt),
		.shamt(shamt),
		.new_signal(new_signal),
		.write_mem(write_mem_ID),

		.reg_Rs(reg_Rs),
		.reg_Rt(reg_Rt),

		.write_reg1(write_reg_ID1),
		.write_reg2(write_reg_ID2),
		.write_reg3(write_reg_ID3),
		.reg_dst(reg_dst),
		.write_data_in(read_data2),

		.write_back_EXE(write_back_EXE),
		.reg_Rd_EXE(write_reg_EXE),
		.write_back_MEM(write_back_MEM),
		.reg_Rd_MEM(write_reg_MEM),

		.fwd_write_data_EXE(alu_result_EXE),
		.fwd_write_data_MEM(write_data_reg),

		.branch(branch),
		.bne(bne),
		.jr(jr),
		.j_address_in(j_address),
		.j_address(j_address_final),

		.write_reg(write_reg_EXE_in),
		.write_data(write_data),
		.alu_result(alu_result_in_EXE),
		.branch_pc(branch_pc),
		.PCsrc(PCsrc),
		.pc(pc_ID)
	);
	MEMStage MEMStage(
		.clk(clk),
		.address(alu_result_EXE),
		.write_data(write_data_mem),
		.mem_write_en(write_mem_EXE),
		.byte_length(byte_length_EXE),

		.read_data(read_data_mem_in)
	);
	WBStage WBStage(
		.clk(clk),
		.mem_to_reg(mem_to_reg_MEM),
		.alu_result(alu_result_MEM),
		.read_data_mem(read_data_mem),
		.immediate(immediate_MEM),
		.upp_imm(upp_imm_MEM),
		.write_reg_in(write_reg_in_MEM),
		.pc_8(pc_8_MEM),
		.link(link_MEM),
		
		.write_data(write_data_reg),
		.write_reg(write_reg_MEM)
	);
	
	
	
	//pipline registers
	IF2ID IF2ID(
		.clk(clk),
		.pc_in(pc_in_IF),
		.instruction_in(inst_in),
		.pc(pc_IF),
		.instruction(inst),
		.reg_IF2ID_write(reg_IF2ID_write),
		.pc_8_in(pc_8_in_IF),
		.pc_8(pc_8_IF)
	);
	ID2EXE ID2EXE(
		.clk(clk),
		.read_data1_in(read_data1_in),
		.read_data2_in(read_data2_in),
		.read_data3_in(read_data3_in),
		.read_data1F_in(read_data1F_in),
		.read_data2F_in(read_data2F_in),
		.reg_float_in(reg_float_in),
		.alu_op_in(alu_op_in),
		.write_back_in(write_back_in_ID),
		.alu_src_in(alu_src_in),
		.use_shamt_in(use_shamt_in),
		.shamt_in(shamt_in),
		.write_mem_in(write_mem_in_ID),
		.mem_to_reg_in(mem_to_reg_in_ID),
		.write_reg_in1(inst[20:16]),
		.write_reg_in2(inst[15:11]),
		.write_reg_in3(inst[10:6]),
		.immediate_in(immediate_in_ID),
		.reg_dst_in(reg_dst_in),
		.branch_in(branch_in),
		.bne_in(bne_in),
		.pc_in(pc_IF),
		.jump_in(jump_in),
		.jr_in(jr_in),
		.j_address_in(j_address_in),
		.reg_Rs_in(reg_Rs_in),
		.reg_Rt_in(reg_Rt_in),
		.new_signal_in(new_signal_in),
		.byte_length_in(byte_length_in_ID),
		.upp_imm_in(upp_imm_in_ID),
		.link_in(link_in_ID),
		.pc_8_in(pc_8_IF),

		.read_data1(read_data1),
		.read_data2(read_data2),
		.read_data3(read_data3),
		.read_data1F(read_data1F),
		.read_data2F(read_data2F),
		.reg_float(reg_float),
		.immediate(immediate_ID),
		.alu_op(alu_op),
		.write_back(write_back_ID),
		.alu_src(alu_src_ID),
		.use_shamt(use_shamt),
		.shamt(shamt),
		.write_mem(write_mem_ID),
		.mem_to_reg(mem_to_reg_ID),
		.write_reg1(write_reg_ID1),
		.write_reg2(write_reg_ID2),
		.write_reg3(write_reg_ID3),
		.reg_dst(reg_dst),
		.branch(branch),
		.bne(bne),
		.pc(pc_ID),
		.jump(jump),
		.jr(jr),
		.j_address(j_address),
		.reg_Rs(reg_Rs),
		.reg_Rt(reg_Rt),
		.new_signal(new_signal),
		.byte_length(byte_length_ID),
		.upp_imm(upp_imm_ID),
		.link(link_ID),
		.pc_8(pc_8_ID)
	);
	EXE2MEM EXE2MEM(
		.clk(clk),
		.alu_result_in(alu_result_in_EXE),
		.write_data_mem_in(write_data),
		.write_mem_in(write_mem_ID),
		.write_back_in(write_back_ID),
		.mem_to_reg_in(mem_to_reg_ID),
		.write_reg_in(write_reg_EXE_in),
		.byte_length_in(byte_length_ID),
		.immediate_in(immediate_ID),
		.upp_imm_in(upp_imm_ID),
		.link_in(link_ID),
		.pc_8_in(pc_8_ID),

		.alu_result(alu_result_EXE),
		.write_mem(write_mem_EXE),
		.write_data_mem(write_data_mem),
		.write_back(write_back_EXE),
		.mem_to_reg(mem_to_reg_EXE),
		.write_reg(write_reg_EXE),
		.byte_length(byte_length_EXE),
		.immediate(immediate_EXE),
		.upp_imm(upp_imm_EXE),
		.link(link_EXE),
		.pc_8(pc_8_EXE)
	);
	
	MEM2WB MEM2WB(
		.clk(clk),
		.read_data_mem_in(read_data_mem_in),
		.alu_result_in(alu_result_EXE),
		.write_reg_in(write_reg_EXE),
		.write_back_in(write_back_EXE),
		.mem_to_reg_in(mem_to_reg_EXE),
		.immediate_in(immediate_EXE),
		.upp_imm_in(upp_imm_EXE),
		.link_in(link_EXE),
		.pc_8_in(pc_8_EXE),

		.read_data_mem(read_data_mem),
		.alu_result(alu_result_MEM),
		.write_reg(write_reg_in_MEM),
		.write_back(write_back_MEM),
		.mem_to_reg(mem_to_reg_MEM), 
		.immediate(immediate_MEM),
		.upp_imm(upp_imm_MEM),
		.link(link_MEM),
		.pc_8(pc_8_MEM)
	);
	
	
endmodule
	