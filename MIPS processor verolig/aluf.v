module aluAddF(in1,in2,result);
	`include "param.v"
	input [`widthReg-1:0]in1,in2;
	output [`widthReg-1:0] result;
	reg in1_sign,in2_sign,result_sign;
	reg [24:0] in1_frac,in2_frac,result_frac;
	reg [7:0] in1_exp,in2_exp,result_exp;
	reg [24:0] shiftAmount,count;
	assign result[31] = result_sign;
	assign result[30:23] = result_exp;
	assign result[22:0] = result_frac;
	always@(*)begin 
		in1_sign = in1[31];
		in2_sign = in2[31];
		in1_exp = in1[30:23];
		in2_exp = in2[30:23];
		if(in1[30:23] == 8'b0)begin
			in1_frac = {2'b00,in1[22:0]}; //hidden bit = 0
		end else begin
			in1_frac = {2'b01,in1[22:0]}; //hidden bit = 1
		end
		
		if(in2[30:23] == 8'b0)begin
			in2_frac = {2'b00,in2[22:0]};
		end else begin
			in2_frac = {2'b01,in2[22:0]};
		end		
	//Case 1: equal exponents	
	if(in1_exp == in2_exp)begin 
		result_exp = in1_exp;
		if(in1_sign == in2_sign) begin
			result_frac = in1_frac + in2_frac;
			result_sign = in1_sign;
		end 
		else begin 
			//take sign of bigger number
			if(in1_frac > in2_frac)begin
				result_frac = in1_frac - in2_frac; 
				result_sign = in1_sign;
			end 
			else begin
				result_frac = in2_frac - in1_frac;
				result_sign = in2_sign;
			end
		end
	end 
	else begin 
	//Case 2: unequal exponents, shift smaller number 	
		if(in1_exp > in2_exp)begin
			result_exp = in1_exp;
			result_sign = in1_sign;
			shiftAmount = in1_exp - in2_exp;
			in2_frac = in2_frac >> shiftAmount;
			 if (in1_sign == in2_sign)
				result_frac = in1_frac + in2_frac;
        else
          	result_frac = in1_frac - in2_frac;
		end
		else begin 
			result_exp = in2_exp;
			result_sign = in2_sign;
			shiftAmount = in2_exp - in1_exp;
			in1_frac = in1_frac >> shiftAmount;
         if (in1_sign == in2_sign)
				result_frac = in1_frac + in2_frac;
        else
          	result_frac = in2_frac - in1_frac;
		end
	end
	
	//normalize result
	//Case 1: result is larger, shift right one bit 
	 if(result_frac[24] == 1) begin  
      result_exp = result_exp + 1;
      result_frac = result_frac >> 1;
	 end
	 //Case 2: result is smaller, shift left until hidden bit is 1
	 else if(result_frac[23] == 0)begin
		count = 0;
		while((result_frac[23] == 0) && count < 100)begin
			result_exp = result_exp - 1;
			result_frac = result_frac << 1;
			count = count +1;
		end
		
	 end
	end
endmodule
