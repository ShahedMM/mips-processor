module WBStage(clk,mem_to_reg,alu_result,read_data_mem,write_data,upp_imm,immediate,write_reg_in,write_reg,pc_8,link);
	`include "param.v"
  input clk;
  input [`widthReg-1:0] alu_result, read_data_mem, immediate, pc_8;
  input mem_to_reg, upp_imm, link;
  input [`selReg-1:0] write_reg_in;

  output [`widthReg-1:0] write_data;
  output [`selReg-1:0] write_reg;

  wire [`widthReg-1:0] write_data_tmp,write_data_tmp2;

	mux2_1 mux2_1WB(
		.sel(mem_to_reg),
		.in1(alu_result),
		.in2(read_data_mem),
		.out(write_data_tmp)
	);

	mux2_1 mux2_1WB2(
		.sel(upp_imm),
		.in1(write_data_tmp),
		.in2({immediate[15:0],16'b0}),
		.out(write_data_tmp2)
	);

	mux2_1 mux2_1WB3(
		.sel(link),
		.in1(write_data_tmp2),
		.in2(pc_8),
		.out(write_data)
	);

	mux2_1_5_bit mux2_1WB4(
		.sel(link),
		.in1(write_reg_in),
		.in2(5'b11111),
		.out(write_reg)
	);
	
endmodule
