module IF2ID(clk, pc_in, instruction_in, pc, instruction,reg_IF2ID_write,pc_8_in,pc_8);
	`include "param.v"
  input clk,reg_IF2ID_write;
  input [`widthReg-1:0] pc_in, instruction_in, pc_8_in;
  output reg [`widthReg-1:0] pc, instruction, pc_8;


  always @ (posedge clk ) begin
 	if(reg_IF2ID_write != 0)
 		begin
	    instruction <= instruction_in;
	    pc <= pc_in;
	    pc_8 <= pc_8_in;
	    end

	//$monitor("IF2ID pc: %d",pc);
  end
endmodule // IF2ID