module aluDiv(in1,in2,result);
	`include "param.v"
	input [`widthReg-1:0] in1,in2;
	reg [`widthReg-1:0] quotient,remainder;

	
	output reg [`widthReg+`widthReg-1:0]result;
	
	reg [`widthReg-1:0]in1_tmp,in2_tmp;
	reg sign,sign_in1;
	
	/*
		- first 32 bits of result constains the quotient in1/in2
		- second 32 bits of result contains the remainder in1%in2
		- signed division:
		1) save the sign of the answer and the sign of in1
		2)	find two's complemnet of any negative number
		3) divide positive numbers
		4) change sign of answer as necessary
		5) if in1 < 0, flip sign of remainder
	*/
	always@(*)begin
		  sign = in1[`widthReg-1] ^ in2[`widthReg-1]; //if sign ==1 then answer will be negative
		  sign_in1 = in1[`widthReg-1];
        if(in1[`widthReg-1] == 1'b1) 
          in1_tmp = ~in1 + 1'b1; 
        else in1_tmp = in1;
        if(in2[`widthReg-1] == 1'b1)
          in2_tmp = ~in2 + 1'b1;
        else in2_tmp = in2;
        quotient = in1_tmp/in2_tmp; 
        remainder = in1_tmp%in2_tmp;
		  if(sign == 1'b1)
			quotient = ~quotient +1'b1;
		  if(sign_in1 == 1'b1)
			remainder = ~remainder +1'b1;
			
			result = {remainder,quotient};
	end
endmodule