module mux3_1_data(sel,in1,in2,in3,out);
	`include "param.v"
	input [`widthReg-1:0] in1,in2,in3;
	input [1:0] sel;
	
	output reg [`widthReg-1:0] out;
	
	always@(sel or in1 or in2 or in3) begin
		if(sel == 2'b0)
			out = in1;
		else if(sel == 2'b01)
			out = in2;
		else if(sel == 2'b10)
			out = in3;
	end
endmodule