module MEM2WB(clk,read_data_mem_in,read_data_mem,alu_result_in,alu_result,write_reg_in,write_reg,write_back_in,write_back,mem_to_reg_in,mem_to_reg,upp_imm_in,upp_imm,immediate_in,immediate,pc_8_in,pc_8,link_in,link);
	`include "param.v"
  input clk;
  input [`widthReg-1:0] read_data_mem_in,alu_result_in,immediate_in,pc_8_in;
  input [`selReg-1:0] write_reg_in;
  input mem_to_reg_in,upp_imm_in,link_in;
  input [1:0]write_back_in;
  
  output reg [`widthReg-1:0] read_data_mem,alu_result,immediate,pc_8;
  output reg [`selReg-1:0] write_reg;
  output reg mem_to_reg,upp_imm,link;
  output reg [1:0]write_back;
  
	always@(posedge clk)begin
		read_data_mem <=  read_data_mem_in;
		alu_result <= alu_result_in; 
		write_reg <= write_reg_in;
		mem_to_reg <= mem_to_reg_in;
		write_back <= write_back_in;
		immediate <= immediate_in;
		upp_imm <= upp_imm_in;
		link <= link_in;
	 	pc_8 <= pc_8_in;

	end
	 
endmodule
