module control(opcode,func,alu_op,write_back,write_mem,alu_src,use_shamt,mem_to_reg,reg_dst,branch,bne,jump,reg_float,sign_ext_imm,new_signal,byte_length,upp_imm,jr,link);
	`include "param.v"
	input [`opcode_func-1:0] opcode,func;
	output reg [`aluOp-1:0]alu_op;
	output reg write_mem,alu_src,mem_to_reg,branch,jump,bne,reg_float,use_shamt,sign_ext_imm,new_signal, byte_length,upp_imm,jr,link;
	output reg [1:0] reg_dst,write_back;

	always@(opcode or func) begin
		alu_op = 0;
		write_back = 0;
		write_mem = 0;
		alu_src = 0;
		use_shamt = 0;
		mem_to_reg = 0;
		reg_dst = 0;
		branch = 0; 
		jump = 0;
		bne = 0;
		reg_float = 0;
		sign_ext_imm = 1;
		new_signal = 0;
		byte_length = 0;
		upp_imm = 0;
		jr = 0;
		link = 0;
		case(opcode)
			`R: 
				case(func)
					`AddReg: begin
						alu_op = `Add;
						write_back = 01;
						reg_dst = 01;
					end
					`OrReg: begin
						alu_op = `Or;
						write_back = 01;
						reg_dst = 01;
					end
					`NorReg: begin
						alu_op = `Nor;
						write_back = 01;
						reg_dst = 01;
					end
					`SltReg: begin
						alu_op = `Slt;
						write_back = 01;
						reg_dst = 01;
					end
					`SltuReg: begin
						alu_op = `Sltu;
						write_back = 01;
						reg_dst = 01;
					end
					`SllReg: begin
						alu_op = `Sll;
						write_back = 01;
						reg_dst = 01;
						use_shamt = 1;
					end
					`SlrReg: begin
						alu_op = `Slr;
						write_back = 01;
						reg_dst = 01;
						use_shamt = 1;
					end
					`AndReg: begin
						alu_op = `And;
						write_back = 01;
						reg_dst = 01;
					end
					`SubReg: begin
						alu_op = `Sub;
						write_back = 01;
						reg_dst = 01;
					end
					`SubuReg: begin
						alu_op = `Subu;
						write_back = 01;
						reg_dst = 01;
					end
					`MoveHigh: begin
						alu_op = `Mfhi;
						write_back = 01;
						reg_dst = 01;
					end
					`MoveLow: begin
						alu_op = `Mflo;
						write_back = 01;
						reg_dst = 01;
					end
					`Multiply: begin
						alu_op = `Mult;
					end
					`Divide: begin
						alu_op = `Div;
					end
					`LwNew: begin
						alu_op=`Add;
						write_back = 01;
						mem_to_reg =1;
						new_signal = 1;
					end
					`SwNew: begin
						alu_op=`Add;
						write_mem = 1;
						new_signal = 1;
					end
					`JumpReg: begin
						jump = 1;
						jr = 1;
					end
				endcase
			`AddImmediate: begin
				alu_op = `Add;
				write_back = 01;
				alu_src = 1;
			end
			`AddImmediateUnsigned: begin
				alu_op = `Addu;
				write_back = 01;
				alu_src = 1;
			end
			`AndImmediate: begin
				alu_op = `And;
				write_back = 01;
				alu_src = 1;
				sign_ext_imm = 0;
			end
			`OrImmediate: begin
				alu_op = `Or;
				write_back = 01;
				alu_src = 1;
				sign_ext_imm = 0;
			end
			`LoadWord: begin
				alu_op=`Add;
				write_back = 01;
				alu_src = 1;
				mem_to_reg =1;
			end
			`LoadByteUnsigned: begin
				alu_op=`Add;
				write_back = 01;
				alu_src = 1;
				mem_to_reg =1;
				byte_length = 1;
			end
			`LoadUpperImm: begin
				alu_op=`Add;
				write_back = 01;
				alu_src = 1;
				mem_to_reg =1;
				upp_imm = 1;
			end
			`StoreWord: begin
				alu_op=`Add;
				write_mem = 1;
				alu_src = 1;
			end
			`SotreByte: begin
				alu_op=`Add;
				write_mem = 1;
				alu_src = 1;
				byte_length = 1;
			end
			`Branch: begin
				alu_op=`Sub; 
				branch = 1; 
			end
			`BranchNotEqual: begin 
				alu_op=`Sub;
				bne = 1;
			end
			`Jump: begin  
				jump = 1;
			end
			`JumpAndLink: begin  
				jump = 1;
				link = 1;
				write_back = 01;
			end
			`AddFloat: begin
				reg_float = 1;
				write_back = 10;
				reg_dst = 10;
				alu_op = `AddF;
			end
			`LoadWordF: begin
				alu_op = `Add;
				write_back = 10;
				alu_src = 1;
				mem_to_reg = 1;
			end
		endcase
	end

endmodule	
						
	
	