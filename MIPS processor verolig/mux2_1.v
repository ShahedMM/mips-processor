module mux2_1(sel,in1,in2,out);
	`include "param.v"
	input [`widthReg-1:0] in1,in2;
	input sel;
	
	output reg [`widthReg-1:0] out;
	
	always@(sel or in1 or in2) begin
		if(sel == 1'b0)
			out = in1;
		else out = in2;
	end
endmodule