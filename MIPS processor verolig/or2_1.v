module or2_1(in1,in2,out);
	`include "param.v"
	input in1,in2;
	
	output reg out;
	
	always@(in1 or in2) begin
			out = in1 | in2;
	end
endmodule