module adder2_1 (in1, in2, out);
	`include "param.v"
  input [`widthReg-1:0] in1,in2;
  output [`widthReg-1:0] out;
  assign out = (in1 + in2);
endmodule // adder