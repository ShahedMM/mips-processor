module IDStage(clk,inst,reg_write,reg_write_en,write_data,read_data1,read_data2,read_data3,immediate,j_address,alu_op,write_back,write_mem,alu_src,use_shamt,shamt,mem_to_reg,reg_dst,branch,jump,pc,bne,read_data1F,read_data2F,reg_float,reg_Rs,reg_Rt,reg_write_EXE,mem_to_reg_EXE,pc_write,reg_IF2ID_write,new_signal,byte_length,upp_imm,jr,link);
	`include "param.v"
	input clk,mem_to_reg_EXE;
	input [`widthReg-1:0]inst,write_data,pc;
	input [`selReg-1:0]reg_write,reg_write_EXE;
	input [1:0] reg_write_en;
	
	output [`widthReg-1:0]read_data1,read_data2,read_data3,immediate,j_address,read_data1F,read_data2F,shamt;
	output [`aluOp-1:0]alu_op;
	output write_mem,alu_src,mem_to_reg,jump,branch,bne,reg_float,pc_write,reg_IF2ID_write,use_shamt,new_signal,byte_length,upp_imm,jr,link;
	output [1:0]reg_dst,write_back;
	output [`selReg-1:0] reg_Rs,reg_Rt;
	
	wire [`widthReg-1:0] sign_ext_immediate;

	wire writeOnRegFileFloat = (reg_write_en == 2'b10);
	wire writeOnRegFile = (reg_write_en == 2'b01);

	wire write_back_in,write_mem_in,signExtImm;
	
	assign reg_Rs = inst[25:21];
	assign reg_Rt = inst[20:16];
	assign shamt = {27'b0,inst[10:6]};

	hazardUnit hazardControlUnit1(
		.reg_Rs_ID(reg_Rs),
		.reg_Rt_ID(reg_Rt),
		.reg_Rt_EXE(reg_write_EXE),
		.mem_read_EXE(mem_to_reg_EXE),//is lw in exe?

		.write_back_in(write_back_in),
		.write_mem_in(write_mem_in),//sw

		.write_back(write_back),
		.write_mem(write_mem),
		
		.pc_write(pc_write),
		.reg_IF2ID_write(reg_IF2ID_write)
	);
	

	regFileFloat regFileFloatID(
		.clk(clk),
		.read_reg1(inst[20:16]),
		.read_reg2(inst[15:11]),
		.reg_write(reg_write),
		.reg_write_en(writeOnRegFileFloat),
		.write_data(write_data),
		.read_data1(read_data1F),
		.read_data2(read_data2F)
	);
	
	
	regFile regFileID(
		.clk(clk),
		.read_reg1(inst[25:21]),
		.read_reg2(inst[20:16]),
		.read_reg3(inst[15:11]),
		.reg_write(reg_write),
		.reg_write_en(writeOnRegFile),
		.write_data(write_data),
		.read_data1(read_data1),
		.read_data2(read_data2),
		.read_data3(read_data3)
	);
	
	signExtend signExtendID(
		.in(inst[15:0]),
		.out(sign_ext_immediate)
	);	

	mux2_1 extImm(
		.sel(signExtImm),
		.in1({16'b0,inst[15:0]}),
		.in2(sign_ext_immediate),
		.out(immediate)
	);
	
	shiftLeft j_addressID(
		.in({2'b0,pc[31:28],inst[25:0]}),
		.bits(2),
		.out(j_address)
	);

	control controlID(
		.opcode(inst[31:26]),
		.func(inst[5:0]),

		.alu_op(alu_op),
		.write_back(write_back_in),//in
		.write_mem(write_mem_in),//in
		.alu_src(alu_src),
		.use_shamt(use_shamt),
		.mem_to_reg(mem_to_reg),//in sel of last mux
		.reg_dst(reg_dst),
		.branch(branch),
		.sign_ext_imm(signExtImm),
		.bne(bne),
		.jump(jump),
		.reg_float(reg_float),
		.new_signal(new_signal),
		.byte_length(byte_length),
		.upp_imm(upp_imm),
		.jr(jr),
		.link(link)
	);
	
endmodule
	
