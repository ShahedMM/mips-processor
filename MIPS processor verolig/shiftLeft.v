module shiftLeft(in,bits,out);
	`include "param.v"
	input [`widthReg-1:0] in,bits;
	output [`widthReg-1:0] out;
	assign out = in << bits;
endmodule
