module IFStage(clk,pc,inst,branch_pc,PCsrc,pc_next,j_address,jump,pc_write,pc_8);
	`include "param.v"
	input clk,PCsrc,jump,pc_write;
  input [`widthReg-1:0] branch_pc,j_address;
  
	output [`widthReg-1:0] pc,inst, pc_next, pc_8;
	wire [`widthReg-1:0] pc_next,pc_taken_1,pc_taken_2;


	adder add4 (
    .in(pc),
    .out(pc_next)
  );

  adder add8 (
    .in(pc_next),
    .out(pc_8)
  );
	
	 register PCReg (
    .clk(clk),
    .regIn(pc_taken_2),
    .regOut(pc),
    .en(pc_write)
  );

  pc_mux PCSelection(
    .sel(PCsrc),
    .in1(pc_next),
    .in2(branch_pc),
    .out(pc_taken_1)
  );

  pc_mux PCSelectionJump(
    .sel(jump),
    .in1(pc_taken_1),
    .in2(j_address),
    .out(pc_taken_2)
  );
  
   instructionMem instructions (
    .addr(pc),
    .instruction(inst)
  );
endmodule
