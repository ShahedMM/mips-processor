module hazardUnit(reg_Rs_ID,reg_Rt_ID,reg_Rt_EXE,mem_read_EXE,write_back_in,write_mem_in,write_back,write_mem,pc_write,reg_IF2ID_write);
	`include "param.v"
	input [`selReg-1:0]  reg_Rs_ID,reg_Rt_ID,reg_Rt_EXE;
	input mem_read_EXE;
	
	input [1:0] write_back_in;
	input write_mem_in;

	output reg [1:0] write_back;
	output reg write_mem,pc_write,reg_IF2ID_write;

	reg stall;

	always @(*)begin
		stall = 0;
		if(mem_read_EXE==1 && (reg_Rs_ID == reg_Rt_EXE || reg_Rt_ID == reg_Rt_EXE))
			stall = 1;

		write_back = write_back_in & (!stall);
		write_mem = write_mem_in & (!stall);

		pc_write = !stall;
		reg_IF2ID_write = !stall;

	end
endmodule
