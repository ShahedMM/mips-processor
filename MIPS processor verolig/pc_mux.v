module pc_mux(sel,in1,in2,out);
	`include "param.v"
	input [`widthReg-1:0]in1,in2;
	input sel;
	
	output reg [`widthReg-1:0] out;
	
	always@(sel or in1 or in2) begin
		if(sel == 1'b1)
			out = in2;
		else out = in1;
	end
endmodule