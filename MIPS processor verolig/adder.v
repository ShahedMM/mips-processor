module adder (in, out);
	//`include "param.v"
  input [31:0] in;
  output [31:0] out;
  wire [31:0] sum;
  assign sum = (in + 32'h00000004 );
  assign out = (sum > 32'h00000100) ? 32'h00000000 : sum;
endmodule // adder