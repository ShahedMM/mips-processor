
module instructionMem (addr, instruction);
  
  input [31:0] addr;
  output [31:0] instruction;

	reg [7:0] instMem [0:1023];
	
  initial begin

        
       /*   instMem[0] = 8'b00100100; //-- addi t0,zero,4
          instMem[1] = 8'b00001000;
          instMem[2] = 8'b00000000;
          instMem[3] = 8'b00000100;*/
			 
    		 /*instMem[0] = 8'b00100100; //-- addi t0,zero,4
          instMem[1] = 8'b00001000;
          instMem[2] = 8'b00000000;
          instMem[3] = 8'b00000100;*/
			 {instMem[0], instMem[1],instMem[2],instMem[3]} = 32'h46031040;
    		
  end
  
    assign instruction = {instMem[addr], instMem[addr+1], instMem[addr+2], instMem[addr+3]};
endmodule // insttructionMem