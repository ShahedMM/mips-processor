module aluMult(in1,in2,result);
	`include "param.v"
	input [`widthReg-1:0]in1,in2;
	output reg [`widthReg+`widthReg-1:0]result;
	
	reg [`widthReg-1:0]in1_tmp,in2_tmp;
	reg sign;
	/*the * operator does unsigned multiplication so to multiply signed numbers:
	1) save the sign of the answer 
	2)find two's complemnet of any negative number
	3) multiply positive numbers
	4) change sign of answer as necessary
	*/
	always@(*)begin
		  sign = in1[`widthReg-1] ^ in2[`widthReg-1]; //if sign ==1 then answer will be negative
        if(in1[`widthReg-1] == 1'b1) 
          in1_tmp = ~in1 + 1'b1; 
        else in1_tmp = in1;
        if(in2[`widthReg-1] == 1'b1)
          in2_tmp = ~in2 + 1'b1;
        else in2_tmp = in2;
        result = in1_tmp*in2_tmp; 
        if(sign == 1'b1)
        result = ~result +1'b1; 
	end
endmodule
	