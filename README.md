# MIPS Processor

The project is an implementation of a pipelined MIPS processor featuring hazard detection as well as forwarding.

The implementation is based on a limited ISA provided by the lecturer (you can find the ISA in the CUSTOM MIPS excel file). **Verilog HDL** is used.

It was done as part of the Computer Architecture & Organization (2) course at Princess Sumaya University for Technology. 

